Lab #1 Score:

XX / 5 : Notes
    
        Full points are awarded for notes that seem sufficient to guide
        someone else to perform the same activities

XX / 5 : Results

        Full points are awarded for turning in a directory that contains
        outputs from a run of the experiment

XX / 5 : Reflection

        One point for each of the four reflection questions; partial credit
        may be awarded for cursory or incomplete answers. One point for
        overall thoughtfulness of responses

======
Total score: XX / YY
