# How to reproduce Remy results in CloudLab

0) Start a new experiment with this profile: [OnePC-Ubuntu14](https://www.cloudlab.us/p/04a3e6a1-7d83-11e4-afea-001143e453fe)

1) Install required packages:

	sudo apt-get update
	sudo apt-get install libprotobuf-dev libx11-dev xorg-dev protobuf-compiler python-matplotlib inkscape gnuplot

2) Download remy-reproduce-1.0.tar.gz (61 MiB). This is a modified version of
ns-allinone 2.35.
	
	wget http://web.mit.edu/remy/remy-reproduce-1.0.tar.gz

2.1) Download patch:

	wget https://gitlab.flux.utah.edu/cs6963-s15/lab1/raw/master/abs.patch

3) Unpack the tarball:

	tar zxvf remy-reproduce-1.0.tar.gz

4) Change to tarball directory 

	cd remy-reproduce-1.0

4.1) Apply patch:

	patch -p1 < ../abs.patch

4.2) Re-build some header files:

	cd ns-2.35/tcp/remy/
	protoc --cpp_out=. dna.proto
	cd ../../../

5) Run `./install` This will compile the components of ns-allinone, including
our modified ns-2.35.

6) If successful, `cd ns-2.35/tcl/ex/congctrl`

7) Run `./run-figure4-parallel`. By default, this runs the simulations
necessary to construct Figure 4 (a 15 Mbps dumbbell network with RTT of 150
millisecond).  This configuration is contained in
remyconf/dumbbell-buf1000-rtt150-bneck15.tcl. The remyconf directory also
includes the configurations used for the other figures. On a typical PC, the
full job will take a long time to complete. It will run the simulations in
parallel and in the background. After it has completed at least a few runs, you
can start to graph the results.

8) Change to directory where graphing scripts live:

	cd ../graphing-scripts

9) Run `./graphmaker ../congctrl/results/` This will output an SVG and PNG file
for each nsrc (maximum degree of multiplexing) enumerated in
run-figure4-parallel. (As distributed, only 8 is tested.) The graphmaker
requires inkscape.

10)  To view the images, copy them to your local machine. eg.

	scp -r <user>@<host>:remy-reproduce-1.0/ns-2.35/tcl/ex/congctrl/results .
