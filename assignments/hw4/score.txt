HW #4 Score:

XX / 3 : Question 1
    One point for application level events; example answers include logfiles
        and application-level event monitoring (printf, more sophisticated
        logging, etc.)
    One point for socket-level activity: Logging in the application is an
        okay response, as are kernel logging tools like strace and drtrace
    One point for packets on the wire: example answers include tcpdump
        and wireshark

XX / 3 : Question 2
    Lose one point for each incorrect answer (up to three)
    a: Bar
    b: Line
    c: Bar
    d: Line

XX / 4 : Question 3
    One point each for identifying problems with the four graphs
    Graph a: Units not specified, no confidence intervals, lines not
        labeled, no numbers on axes
    Graph b: Violates 3/4 rule (scaling), no confidence intervals, Y axis not
        labeled
    Graph c: too many alternatives presented, lines not labeled, no
        units on y axis, no numbers on either axis, no confidence intervals
    Graph d: categorical data on x axis in arbitrary order, no values
        labeled on y axis

======
Total score: XX / XX

