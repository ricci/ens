Weekly Report #1 Score:

XX / 10 : Submitted a weekly report with commit history to back it up

    Full points are awarded for submitting a report conforming to the
    requested format, along with sufficient evidence in the form of files
    committed to git to back up the claims of the report. Partial points may
    be awarded if the report is incomplete, or if there is no evidence in the
    repository of the claimed progress.

======
Total score: XX / YY
