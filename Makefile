SUBDIRS=syllabus schedule assignments lectures handouts

include Makerules

#
# Build all PDFs for the class and collect them in one directory
#
.PHONY: class install

class: all
	-rm -rf class/
	mkdir class/
	./makeclass

# Simple SCP of all files to Rob's website
install: class
	scp -p class/* web.flux.utah.edu:wp-ricci/content/ens
