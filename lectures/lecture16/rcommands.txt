x = c(1.2, 2.4, 2.9, 4.0, 4.8, 6.1, 7.2, 7.9, 9.3)
y = c(1.1, 2.2, 3.1, 4.4, 5.0, 5.1, 7.2, 8.3, 9.1)

plot(x,y, cex=5)

b1 = (sum(x*y) - length(x)*mean(x)*mean(y))/(sum(x^2) - length(x)*(mean(x)^2))
b0 = mean(y) - b1*mean(x)

abline(b0,b1, lwd=5)

f <- function(x) b0 + b1*x
plot(x,sapply(x,f), cex=5)

r = y - sapply(x,f)

plot(x,r, cex=5)

-----

x = seq(1,100,by=1)
y = sapply(x, function(x) (x^1.4 + rnorm(1,0,10)))

plot(x,y, cex=5)

b1 = (sum(x*y) - length(x)*mean(x)*mean(y))/(sum(x^2) - length(x)*(mean(x)^2))
b0 = mean(y) - b1*mean(x)

abline(b0,b1, lwd=5)

f <- function(x) b0 + b1*x
plot(x,sapply(x,f), cex=5)

r = y - sapply(x,f)

plot(x,r, cex=5)

-----

lm(y ~ x)

-----

model = lm(y ~ poly(x,2,raw=TRUE))

plot(x,y, cex=5)
lines(x, predict(model, data.frame(x=x)), lwd=5)

r = y - predict(model, data.frame(x=x))
plot(x,r)

r = y - sapply(x, function(x) (-11.26668 + x*3.51530 + x^2*0.02947))
